// decode_png.cpp

#include "decode_png.h"


DecodePNG::DecodePNG()
{

}

// getPNG
// Input, c-style string of the PNG to read.
// Output, 2d bool vector of "walls" being "false".
vector<vector < bool> > DecodePNG::getPNG(const char* filename)
{

	// get everything we need to read the png.
	// the encoded PNG
	std::vector<unsigned char> png;
	// // image data
	std::vector<unsigned char> image;
	// // 2d bool vector we are returning.
	std::vector<std::vector < bool> > returnImage;
	// width and height of the image.
	unsigned width, height;

	//load and decode the image.
	lodepng::load_file(png, filename);
	unsigned error = lodepng::decode(image, width, height, png);

	//if there's an error, display it
	if(error) std::cout << "decoder error " << error << ": " << lodepng_error_text(error) << std::endl;

	cout << "width: " << width << "height: " << height << endl;

	unsigned maxLoc = (width * height * 4);

	// process the image.
	for (int y = 0; y < height; ++y)
	{
		std::vector<bool> row;
    	returnImage.insert(returnImage.begin(), row);

		for (int x = 0; x < width; ++x)
		{
			// // calc current location in the image.
			unsigned curLoc = (y * width) * 4 + x * 4;

			if (curLoc < maxLoc)
			{
				// for each pixel, if below 50 on red element, set as "black" or impassable
				if (image[curLoc] < 50)
				{
					returnImage[0].push_back(false);
				}
				else
				{
					returnImage[0].push_back(true);
				}
			}
		}	
	}
	// // return image vector.
	return returnImage;
}

