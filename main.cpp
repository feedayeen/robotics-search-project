// main.cpp

#include "final_project.h"

int main()
{

	// create RobotControl object.
	cout << "Please enter port: ";
	int port;
	cin >> port;
	double x = 4, y = 4, theta = 0, bottom=-5, left=-5;

	cout << "Enter map filename: ";
	string filename = "givenmap.png";
	
	cout << "Enter map's left corner ";
	cin >> left;

	cout << "Enter map's bottom corner ";
	cin >> bottom;

	cout << "robot x ";
	cin >> x;
	cout << "robot y ";
	cin >> y;
	cout << "robot theta ";
	cin >> theta;

	RobotControl r(port);

	r.openMap(filename, bottom, left);
	
	//run its loop.
	r.mainLoop(x,y,theta);

	return 0;

}


