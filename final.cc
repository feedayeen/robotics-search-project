#include <libplayerc++/playerc++.h>
#include <iostream>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <cmath>
#include <string>
#include <sys/time.h>

using namespace PlayerCc;
using namespace std;

#define PI 3.14159
#define RES 20

//Defined in the local scope because I don't want to pass these parameters to each function
PlayerClient robot("127.0.0.1", 6665);
Position2dProxy pp(&robot,0);
LaserProxy lp(&robot,0);
timeval start, stop, loop, temp;

void time(bool go)
{
	if(go)
	{
		gettimeofday(&start,0);
	}
	else
	{
		gettimeofday(&stop,0);
		cout << "Run time in miliseconds\t" << (stop.tv_sec-start.tv_sec)*1000+((stop.tv_usec-  start.tv_usec)/1000) << endl;
	}
}

class Map
{
private:
	int minX, minY, maxX, maxY;		//Size limits of the array bounds
	unsigned char** grid;	//Array used to store the sensor information
	int** path;				//Array used for the path finding function

//This function will resize the map size to make it larger, if a dimension is smaller (closer to 0,0) then it's ignored
//This will also resize the path array which is used as a temporary array for the path finding functions
	void resize(int MINX, int MINY, int MAXX, int MAXY)
	{
		MINX = std::min(MINX, minX);
		MINY = std::min(MINY, minY);
		MAXX = std::max(MAXX, maxX);
		MAXY = std::max(MAXY, maxY);
//		cout << "New size " << MINX << " " << MINY << " _ " << MAXX << " " << MAXY << endl;
		int width = (MAXX - MINX)*RES;
		int height = (MAXY - MINY)*RES;
		unsigned char** tempGrid = new unsigned char*[width];
		int** tempPath = new int*[width];
		for(int i = 0; i < width; i++)
		{
			tempGrid[i] = new unsigned char[height];
			tempPath[i] = new int[height];
			for(int j = 0; j < height; j++)
			{
				tempGrid[i][j] = 150;
				tempPath[i][j] = 0;
			}
		}
		int dx = (minX-MINX)*RES;
		int dy = (minY-MINY)*RES;
		width = (maxX - minX)*RES;
		height = (maxY - minY)*RES;
//		cout << "deltas " << dx << " " << dy << endl; 
		for(int i = 0; i < width; i++)
		{
			for(int j = 0; j < height; j++)
			{
				tempGrid[i+dx][j+dy] = grid[i][j];
				tempPath[i+dx][j+dy] = path[i][j];
			}
		}
		for(int i = 0; i < width; i++)
		{
			delete[] grid[i];
			delete[] path[i];
		}
		delete[] grid;
		delete[] path;
		grid = tempGrid;
		path = tempPath;
		minX = MINX; minY = MINY; maxX = MAXX; maxY = MAXY;
	}

//The input an x,y index in the map array, returns true if the index exists in the array
	bool valid(int x, int y)
	{
		int width = (maxX - minX)*RES;
		int height = (maxY - minY)*RES;
		if(x >= 0 && y >= 0 && x < width && y < height)
		{
			return true;
		}
		return false;
	}

//This is a recursive function used to calculate the walls, fronteer, and empty regions in the map using a flood-fill algorithm.
	int boardersRec(int x, int y, int cost)
	{
		if(valid(x,y))
		{
			if(grid[x][y] > 150 && path[x][y] == 0)//This is an explored empty region that  hasn't been seen yet, go to it's neighbors
			{
				path[x][y] = cost;
				boardersRec(x-1,y,1);
				boardersRec(x+1,y,1);
				boardersRec(x,y-1,1);
				boardersRec(x,y+1,1);
				return 1;
			}
			else if(grid[x][y] == 150)//This is an unexplored region, call it fog and stop
			{
				path[x][y] = -1;
				return 0;
			}
			else if(grid[x][y] < 150)//this is an impassible region, call it a wall and  stop
			{
				path[x][y] = -2;
				return 0;
			}
		}
	}
	
//Sets the sensor values stored in the grid array at a given coordinate, includes a radius parameter to specify range
	void set(int xIndex, int yIndex, bool wall, int rad)
	{
		for(int x = xIndex-rad; x <= (xIndex+rad); x++)
		{
			for(int y = yIndex-rad; y <= (yIndex+rad); y++)
			{
				if(valid(x,y))
				{
					if(wall)
					{
						if(grid[x][y] > 10)
						{
							grid[x][y] -= 5;
						}
					}
					else
					{
						if(grid[x][y] < 240)
						{
							grid[x][y] += 5;
						}
					}
				}
			}
		}
	}

//Converts the x,y index measured by the getPosition function into x,y coordinate in the arrays
	void index(double x, double y, int& outX, int& outY)
	{
		if(x < minX || y < minY || x >= maxX || y >= maxY)
		{
			resize((int)floor(x)-1,(int)floor(y)-1,(int)ceil(x)+1,(int)ceil(y)+1);
		}
		outX = (int)((x-minX)*RES);
		outY = (int)((y-minY)*RES);
	}
	
public:
	Map()
	{
		minX = 0; minY = 0; maxX = 0; maxY = 0;
		grid = new unsigned char*[0];
		path = new int*[0];
		resize(-1,-1,1,1);
	}
	
//Prints the grid and path arrays to csv files
	void print()
	{
		ofstream g, p;
		g.open("grid.txt");
		p.open("path.txt");
		int width = (maxX - minX)*RES;
		int height = (maxY - minY)*RES;
		for(int j = height-1; j >= 0; j--)
		{
			for(int i = 0; i < width; i++)
			{
				if(i%RES == 0 && j%RES == 0)
				{
					g << 255;
				}
				else
				{
					g << (int)grid[i][j];
				}
				p << (int)path[i][j];
				if(i < (width-1))
				{
					g << ",";
					p << ",";
				}
			}
			g << endl;
			p << endl;
		}
		g.close();
		p.close();
	}

//Calculates the fronteer, walls, and empty from a given x,y coordinate representing the robot's position
//This updates the path array with those values which can then be used by pathfinding functions
	void boundaries(double x, double y)
	{
		int width = (maxX - minX)*RES;
		int height = (maxY - minY)*RES;
		for(int i = 0; i < width; i++)
		{
			for(int j = 0; j < height; j++)
			{
				path[i][j] = 0;
			}
		}
		int xIndex, yIndex;
		index(x,y,xIndex,yIndex);
		boardersRec(xIndex,yIndex,1);
	}

//TODO: This function should output the next region to explore weighed by the robot's current location
	void target(double x, double y)
	{
		int xIndex, yIndex;
		index(x,y,xIndex,yIndex);
		double goalX = xIndex;
		double goalY = yIndex;
		int width = (maxX - minX)*RES;
		int height = (maxY - minY)*RES;
		for(int x = 0; x < width; x++)
		{
			for(int y = 0; y < height; y++)
			{
				if(path[x][y] == -1)//Check if a region is unexplored
				{
					double dx = x-xIndex;
					double dy = y-yIndex ;
					if((dx*dy) != 0)
					{
						goalX += dx/(dx*dx);
						goalY += dy/(dy*dy);
					}
				}
			}
		}
		cout << "Goto " << goalX << " " << goalY << endl;
	}

//Adds sensor information to the grid map
	void sensor(double startX, double startY, double stopX, double stopY, bool wall)
	{
		int startXIndex, startYIndex, stopXIndex, stopYIndex;
		index(startX,startY,startXIndex,startYIndex);
		index(stopX,stopY,stopXIndex,stopYIndex);
		double dx = stopXIndex-startXIndex; double dy = stopYIndex-startYIndex;
		double distance = sqrt((dx*dx)+(dy*dy));
		dx /= distance; dy /= distance;
		for(int i = 0; i < (distance+RES/5); i++)
		{
			int xIndex = (int)(startXIndex + (i*dx));
			int yIndex = (int)(startYIndex + (i*dy));
			if(!wall && i > (distance-1))
			{
				return;
			}
			if(i < distance)
			{
				set(xIndex,yIndex, false, 1);
			}
			else
			{
				set(xIndex,yIndex, true, 1);
			}
		}
	}
};

void read()
{
	robot.Read();
	if(lp.GetCount() == 0)
	{
		while(lp.GetCount() == 0)
			robot.Read();
		return;
	}
	robot.Read();
}

double lasers(Map& map)
{
	read();
	double DX = pp.GetXPos();
	double DY = pp.GetYPos();
	double DR = pp.GetYaw();
	double lr[90][2];
	for(int i = 5; i < 85; i++)
	{
		lr[i][1] = lp[i];
		lr[i][2] = lp.GetBearing(i);
	}
	for(int i = 5; i < 85; i++)
	{
		double x = DX + lr[i][1] * cos(DR + lr[i][2]);
		double y = DY + lr[i][1] * sin(DR + lr[i][2]);
		map.sensor(DX,DY,x,y,(lp[i] < 5 && abs(lp[i-1] - lp[i]) < 0.1 && abs(lp[i+1] - lp[i])  < 0.1));
	}
	double nearest = .75;
	double theta = PI;
	for(int i = 20; i < 70; i++)
	{
		if(nearest > lr[i][1])
		{
			nearest = lr[i][1];
			theta = lr[i][2];
		}
	}
	cout << theta << endl;
	return theta;
}

bool collision()
{
	read();
	double DX = pp.GetXPos();
	double DY = pp.GetYPos();
	double DR = pp.GetYaw();
	double lr[90][2];
	for(int i = 5; i < 85; i++)
	{
		lr[i][1] = lp[i];
		lr[i][2] = lp.GetBearing(i);
	}
	for(int i = 30; i < 60; i++)
	{
		if(.75 > lr[i][1])
		{
			return true;
		}
	}
	return false;
}

bool turnRight()
{
	read();
	double DX = pp.GetXPos();
	double DY = pp.GetYPos();
	double DR = pp.GetYaw();
	double lr[90][2];
	for(int i = 5; i < 85; i++)
	{
		lr[i][1] = lp[i];
		lr[i][2] = lp.GetBearing(i);
	}
	for(int i = 70; i < 85; i++)
	{
		if(2 > lr[i][1])
		{
			return false;
		}
	}
	return true;
}

/*
This function will return true if the goal condition is met, for the lab this means when the Y value  is sufficiently high
*/
bool goalCondition()
{
	if(pp.GetYPos() > 7)
	{
		return true;
	}
	return false;
}

void sleep(int ms)
{
	usleep(1000*ms);
}

int main(int argc, char** argv)
{
	cout << "start " << endl;
	robot.ReadIfWaiting();
	robot.SetDataMode(PLAYER_DATAMODE_PULL);
	robot.SetReplaceRule(true);
	lp.RequestConfigure();
	sleep(1000);
	Map map = Map();
	while(!goalCondition())
	{
		lasers(map);
		pp.SetSpeed(1,0);
		if(collision() || turnRight())
		{
			if(collision())
			{
				cout << "Wall ahead" << endl;
			}
			if(turnRight())
			{
				cout << "Gap ahead" << endl;
			}
			while(collision())
			{
				pp.SetSpeed(0,-.5);
			}
			while(turnRight())
			{
				pp.SetSpeed(0,.5);
			}
		}
	}
	pp.SetSpeed(0,0);
	map.print();
}
