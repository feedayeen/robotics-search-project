#include <iostream> 
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <cmath>
#include <string>
#include <sys/time.h>
#include <libplayerc++/playerc++.h>
#include "map.h"
#include "decode_png.h"

using namespace PlayerCc;
using namespace std;

#define PI 3.14159

class RobotControl
{
private:
	PlayerClient* robot;
	Position2dProxy* pp;
	LaserProxy* lp;
	timeval start, stop, loop, temp;
	list<point> path;
	bool isPathValid;
	Map* map;

	public:
		RobotControl(int port);
	 	void read();
	 	void mainLoop(double x, double y, double theta);
	 	double lasers(Map* map);
	 	void sleep(int ms);
	 	void detectStar(Map* map, double (*laser_points)[90][3], double (*lr)[90][3]);
	 	void openMap(string mapName, double bottom, double left);
	 	void drive();
	 	void spin();
};

