##getPNG notes

1. Create a DecodePNG object


	DecodePNG imageReader


2. Pass a "C-type" string to getPNG, receive a 2d bool vector.


	string mapname = "t.png";
	vector< vector <bool> > image;
	image = imageReader.getPNG(mapname.c_str());

3. Use the 2d bool vector.




##The 2d bool vector:

	image[row][col]

If it is _0_, then the pixel is "black enough".

If it is _1_, then the pixel is "mostly white".


"Rows" start from the bottom, "Cols" start from the left.


Example: a 10x10 PNG, with one "black" pixel.

	1111111111
	1111111111
	1101111111
	1111111111
	1111111111
	1111111111
	1111111111
	1111111111
	1111111111
	1111111111