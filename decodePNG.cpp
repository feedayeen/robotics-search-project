// decodePNG.cpp

// test program to read PNG files.

#include "lodepng.h"
#include <iostream>
#include <vector>

std::vector<std::vector < bool> > getPNG(const char* filename)
{

	// get everything we need to read the png.
	// the encoded PNG
	std::vector<unsigned char> png;
	// // image data
	std::vector<unsigned char> image;
	// // 2d bool vector we are returning.
	std::vector<std::vector < bool> > returnImage;
	// width and height of the image.
	unsigned width, height;

	//load and decode the image.
	lodepng::load_file(png, filename);
	unsigned error = lodepng::decode(image, width, height, png);

	//if there's an error, display it
	if(error) std::cout << "decoder error " << error << ": " << lodepng_error_text(error) << std::endl;


	unsigned maxLoc = (width * height * 4) - 4;


	// process the image.
	for (int xLoc = 0; xLoc < width; ++xLoc)
	{
		std::vector<bool> row;
    	returnImage.push_back(row);

		for (int yLoc = 0; yLoc < height; ++yLoc)
			{




				// // calc current location in the image.
				unsigned curLoc = (xLoc * width) * 4 + yLoc * 4;


				if (curLoc < maxLoc)
				{

					std::cout << "loc: " << curLoc << std::endl;

					// for each pixel, if below 50 on red element, set as "black"
					if (image[curLoc] < 50)
					{
						returnImage[xLoc].push_back(false);
					}
					else
					{
						returnImage[xLoc].push_back(true);
					}
				}

			}	
	}

	// // return image vector.
	return returnImage;
}


int main()
{

	std::vector<std::vector < bool> > image;
	image = getPNG("test.png");
	std::cout << image[300][200];

	return 1;
}