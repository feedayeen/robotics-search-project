// // final_project.cpp

#include "final_project.h"

//Connect to robot software the robot using local host
RobotControl::RobotControl(int port)
{
	cout << "Opening port " << port << endl;
	robot = new PlayerClient("127.0.0.1", port);
	pp = new Position2dProxy(robot,0);
	lp = new LaserProxy(robot,0);
	timeval start, stop, loop, temp;

	//Initialize configuration
	robot->ReadIfWaiting();
	robot->SetDataMode(PLAYER_DATAMODE_PULL);
	robot->SetReplaceRule(true);
	lp->RequestConfigure();
	cout << "start " << endl;
}

//Read sensory inputs, double read is required due to simulation software not updating every other call
void RobotControl::read()
{
	robot->Read();
	if(lp->GetCount() == 0)
	{
		while(lp->GetCount() == 0)
			robot->Read();
		return;
	}
	robot->Read();
}

//Initialize the map
void RobotControl::openMap(string mapname, double bottom, double left)
{
	cout << "opening : " << mapname << " bottom : " << bottom << " left : " << left << endl;
	DecodePNG imageReader;
	vector< vector <bool> > image;


	// put the image into the 2d vector.

	image = imageReader.getPNG(mapname.c_str());

	int rows = image.size();
	int cols = image[0].size();

	map = new Map(image, cols, rows, left, bottom);

	// from -5 -> -4 , at .1 intrv.

		// test testPNGWall

			// print results

	cout << "testing testPNGWall" << endl;

	for (double i = -5; i < -4; i += .1)
	{
		cout << i << ", " << map->testPNGWall(i, 1) << endl;

		/* code */
	}


}

// Function to drive.
void RobotControl::drive()
{
	read();
	double DX = pp->GetXPos();
	double DY = pp->GetYPos();
	double DR = pp->GetYaw();

	bool didWeStall = false;
	// get the list of points based on the current robot status.

	if (path.empty())
	{
		cout << "getting new path." << endl;
		spin();
		pp->SetSpeed(0.0, 0.0);
		path = map->path(DX, DY);


		if (path.size() == 1)
		{
			isPathValid = false;
		}


		isPathValid = true;

		// check if path goes nowhere.
		point lastPoint = path.back();
		
		if (abs(lastPoint.x - DX) < 0.1 && abs(lastPoint.y - DY) < 0.1 )
		{
			// move randomly
			cout << "Path leads to nowhere." << endl;

			int random = rand() % 2;
			int randomTime = (rand() % 150) + 100;

			// move randomly.
			if (random == 0)
			{
				pp->SetSpeed(-0.5, 0.5);
				sleep(randomTime);
			}

			if (random == 1)
			{
				pp->SetSpeed(0.5, 0.5);
				sleep(randomTime);
			}

			read();
			didWeStall = true;
		}

	}

	if (isPathValid == false)
	{
		cout << "trying to get vaild path." << endl;
		sleep(100);
		pp->SetSpeed(0, 0);
		path = map->path(DX, DY);
		
		if (path.size() > 1)
		{	
			cout << "found good path.\n";
			isPathValid = true;
		}
	}
	
	// check if we stalled.
	read();
	bool currentlyStalled = pp->GetStall();
	while(currentlyStalled)
	{
		cout << "Stalled." << endl;
		
		// go random distance back / worth.
		// turn random distanve

		int random = rand() % 2;

		int randomTime = (rand() % 150) + 50;

		// do some stuff.
		if (random == 0)
		{
			pp->SetSpeed(-1, 0.5);
			sleep(randomTime);
		}
		if (random == 1)
		{
			pp->SetSpeed(0.5, 0.5);
			sleep(randomTime);
		}

		read();
		didWeStall = true;
		currentlyStalled = pp->GetStall();
	}

	// handle stalling
	if (didWeStall)
	{
		pp->SetSpeed(0.0, 0.0);
		cout << "stall over, getting new path." << endl;
		read();
		DX = pp->GetXPos();
		DY = pp->GetYPos();
		// DR = pp->GetYaw();
		pp->SetSpeed(0.0, 0.0);
		path = map->path(DX, DY);

		if (path.size() == 1)
		{
			cout << "Invalid path\n";
			isPathValid = false;
		}

	}

	// drive the robot.
	bool haveDrivenRobot = false;

	while(haveDrivenRobot == false)
	{
		if (path.empty())
		{
			cout << "Route Complete" << endl;
			return;
		}

		point currentPoint = path.front();

		if (abs(currentPoint.x - DX) > 0.1 || abs(currentPoint.y - DY) > 0.1 || path.empty())
		{
			// drive to currentPoint
			pp->GoTo(currentPoint.x, currentPoint.y, 0.0);
			haveDrivenRobot = true;
		}
		else
		{
			path.pop_front();
		}
	}
}

//Reads and processes all laser readings
//Walls will be detected and marked on the map
//Laser readings will be processed for the star target
double RobotControl::lasers(Map* map)
{
	read();
	double DX = pp->GetXPos();
	double DY = pp->GetYPos();
	double DR = pp->GetYaw();
	
	// lr[num][0] is unused.
	// lr[num][1] contains laser distance.
	// lr[num][2] contains laser bearing.
	double lr[90][3];

	// 2d double array to store points.
	// points[num][0] -> num's x value.
	// points[num][1] -> num's y value.
	// points[num][2] -> num's chance of being a star point.
	double laser_points[90][3];

	// set points to zero.
	memset(laser_points, 0, sizeof laser_points);

	for(int i = 5; i < 85; i++)
	{
		lr[i][1] = (*lp)[i];
		lr[i][2] = lp->GetBearing(i);
	}
	for(int i = 10; i < 80; i++)
	{
		double x = DX + lr[i][1] * cos(DR + lr[i][2]);
		double y = DY + lr[i][1] * sin(DR + lr[i][2]);

		laser_points[i][0] = x;
		laser_points[i][1] = y;

		map->sensor(DX,DY,x,y,(!map->testPNGWall(x, y)));
	}

	// check if the robot can see the star.
	// also updates map.bin
	detectStar(map, &laser_points, &lr);

	double nearest = .75;
	double theta = PI;
	for(int i = 20; i < 70; i++)
	{
		if(nearest > lr[i][1])
		{
			nearest = lr[i][1];
			theta = lr[i][2];
		}
	}
	return theta;
}

//Uses the law of cosines and hough transform to detect star's inner corner
//Law of cosines calculates the angle between any 3 points from the range finder
//Hough transform reduces false positives from range finder's noise
void RobotControl::detectStar(Map* map, double (*laser_points)[90][3], double (*lr)[90][3])
{
	// start c index at 15, end at 75.
	for (int c = 10; c < 80; ++c)
	{
		// for a, start 5 indexes behind c, end at c.
		for (int a = -5 + c; a < c; ++a)
		{
			// for b, start at index c + 1, end 10 indexes after c.
			for (int b = c + 1; b < 5 + c; ++b)
			{
				// calculate sides of the "triangle" from the laser_points.
				double side_a = sqrt( pow(((*laser_points)[c][0] - (*laser_points)[b][0]), 2.0) + pow(((*laser_points)[c][1] - (*laser_points)[b][1]), 2.0));
				double side_b = sqrt( pow(((*laser_points)[c][0] - (*laser_points)[a][0]), 2.0) + pow(((*laser_points)[c][1] - (*laser_points)[a][1]), 2.0));
				double side_c = sqrt( pow(((*laser_points)[a][0] - (*laser_points)[b][0]), 2.0) + pow(((*laser_points)[a][1] - (*laser_points)[b][1]), 2.0));

				// cout << "sides: " << side_a << ", " << side_b << ", " << side_c << endl;

				// if the sides are "close to" zero or above 0.5 units, continue.
				if (side_a < 0.001 || side_b < 0.001 || side_c < 0.001 || side_a > 0.5 || side_b > 0.5 || side_c > 0.5)
					continue;

				// calculate angle 
				double angle = 0.0;
				angle = acos((pow(side_c, 2.0) - pow(side_a, 2.0) - pow(side_b, 2.0)) / (-2.0 * side_a * side_b));

				// add to bin if angle greater than 1.75r, less than 2.8 r.
				if (angle > 1.75 && angle < 2.8)
				{
					//robot is .25 units
					//filter from map.
					// if a and b have shorter distances than c, add 1 to "score"
					if ((*lr)[a][1] < (*lr)[c][1] && (*lr)[b][1] < (*lr)[c][1] && (*lr)[c][1] > 0.5)
					{

						if (map->testPNGWall((*laser_points)[c][0], (*laser_points)[c][1]))
							continue;
						map->bin((*laser_points)[c][0], (*laser_points)[c][1], angle);
					}

				}
			}
		}
	}
}

//Sleep in miliseconds
void RobotControl::sleep(int ms)
{
	usleep(1000*ms);
}


void RobotControl::spin()
{
	bool complete = false;
	//spin search
	while(pp->GetYaw() < 0 || complete == false)
	{
		lasers(map);
		if(pp->GetYaw() < 0)
		{
			complete = true;
		}
	}
}

void RobotControl::mainLoop(double x, double y, double theta)
{
	cout << "startint robot at " << x << "," << y << " : " << theta << endl;
	//Configure odometry and sync the simulation with client
	pp->SetOdometry(x,y,theta);
	read();
	spin();
	//Begin spin to survay suroundings
	pp->SetSpeed(0,2);
	//stop rotating
	pp->SetSpeed(0,0);
	int i = 0;
	while(!map->star())
	{
		// run the lasers.
		lasers(map);
		// if star not found.
		if (!goalCondition())
		{
			// get path.
			drive();
			// driveTo(location)
		}
	}
	map->print();//Print map
}
