function rgb = map()
%Matlab Script to render the map from the save files
boundary = csvread('path.txt');
grid = csvread('grid.txt');
s = size(boundary);
rgb = zeros(s(1),s(2),3);
rgb(:,:,1) = boundary==-2;
rgb(:,:,2) = boundary==-1;
rgb(:,:,3) = grid/256;
imshow(rgb)
end