// map.cpp

#include "map.h"

//Initializes the map using the results from a loaded PNG file
//The parameters specify the png file's bit values and the size of the map in real world
//Coordinates. left and bottom specify the farthest left and farthest bottom coordinate of the grid
Map::Map(vector< vector<bool> > png, int width, int height, double left, double bottom)
{
	MINX = 0; MINY = 0; MAXX = 0; MAXY = 0;
	PNGWIDTH = width;
	PNGHEIGHT = height;
	PNGLEFT = left;
	PNGBOTTOM = bottom;
	PNG = png;
	GRID = new int*[0];
	PATH = new int*[0];
	BINS = new int*[0];
	resize((int)floor(left)-1,(int)floor(bottom)-1, (int)(1+ceil(left+(width/48.4))),(int)(1+ceil(bottom+(height/48.4))));
	for(int i = 0; i < WIDTH; i++)
	{
		for(int j = 0; j < HEIGHT; j++)
		{
			double realX, realY;
			world(i,j,realX,realY);
			int xInd = (int)((realX-left)*48.4);
			int yInd = (int)((realY-bottom)*48.4);
			if(xInd >= 0 && yInd >= 0 && xInd < PNGWIDTH && yInd < PNGHEIGHT)
			{
				if(!png[yInd][xInd])
				{
					GRID[i][j] = -1000;
				}
			}
		}
	}
}

//Creates a map when no png file is provided
Map::Map()
{
	MINX = 0; MINY = 0; MAXX = 0; MAXY = 0;
	GRID = new int*[0];
	PATH = new int*[0];
	BINS = new int*[0];
	WIDTH = 0;
	HEIGHT = 0;
	// PNG = NULL;
	resize(-1,-1,1,1);
}

//Resizes the map, the new dimension will exceed the previous size
void Map::resize(int min_x, int min_y, int max_x, int max_y)
{
	min_x = std::min(min_x, MINX);
	min_y = std::min(min_y, MINY);
	max_x = std::max(max_x, MAXX);
	max_y = std::max(max_y, MAXY);
	int width = (max_x - min_x)*RES;
	int height = (max_y - min_y)*RES;
	int** tempGrid = new int*[width];
	int** tempPath = new int*[width];
	int** tempBins = new int*[width];
	for(int i = 0; i < width; i++)
	{
		tempGrid[i] = new int[height];
		tempPath[i] = new int[height];
		tempBins[i] = new int[height];
		for(int j = 0; j < height; j++)
		{
			tempGrid[i][j] = 0;
			tempBins[i][j] = 0;
		}
	}
	int dx = (MINX-min_x)*RES;
	int dy = (MINY-min_y)*RES;
	width = (MAXX - MINX)*RES;
	height = (MAXY - MINY)*RES;
	for(int i = 0; i < width; i++)
	{
		for(int j = 0; j < height; j++)
		{
			tempGrid[i+dx][j+dy] = GRID[i][j];
			tempBins[i+dx][j+dy] = BINS[i][j];
		}
	}
	for(int i = 0; i < width; i++)
	{
		delete[] GRID[i];
		delete[] PATH[i];
		delete[] BINS[i];
	}
	delete[] GRID;
	delete[] PATH;
	delete[] BINS;
	GRID = tempGrid;
	PATH = tempPath;
	BINS = tempBins;
	MINX = min_x; MINY = min_y; MAXX = max_x; MAXY = max_y;
	WIDTH = (MAXX - MINX)*RES;HEIGHT = (MAXY - MINY)*RES;
	cout << "Resizing to " << MINX << "," << MINY << " " << MAXX << "," << MAXY << endl;
}

//Identifies if a pixel is a valid coordinate on the occupancy map
bool Map::valid(int x, int y)
{
	if(x >= 0 && y >= 0 && x < WIDTH && y < HEIGHT)
	{
		return true;
	}
	return false;
}

//Sets the value of a pixel in the map in the occupancy map, applies the effect to the neighboring pixels too
void Map::set(int xIndex, int yIndex, bool wall, double dist)
{
	for(int x = xIndex-2; x <= (xIndex+2); x++)
	{
		for(int y = yIndex-2; y <= (yIndex+2); y++)
		{
			if(valid(x,y))
			{
				if(wall)
				{
					GRID[x][y] -= 10;
				}
				else
				{
					GRID[x][y] += 1;
				}
			}
		}
	}
}

//This finds the frontiers in the map which need to be explored
//Fronts is a vector identifying where the frontiers are, it serves as the return value
//Que identifies the farthest regions searched, this is done because a recursive call using a stack has O(n^2) performance
//Pushing the function calls onto a queue has O(n) performance for n tiles.
void Map::boardersRec(vector<point>& fronts, queue<spawn>& que)
{
	while(que.size() != 0)
	{
		spawn s = que.front();
		que.pop();
		int x = s.x;
		int y = s.y;
		int cost = s.cost;
		bool safe = false;
		//Returns true if this is a passable region for the robot, boundary checks for collisions and map extents are performed
		if(valid(x-RAD,y-RAD) && valid(x+RAD,y+RAD))
		{
			safe = true;
			for(int i = x-RAD; i <= (x+RAD) && safe; i++)
			{
				for(int j = y-RAD; j <= (y+RAD) && safe; j++)
				{
					if(GRID[i][j] < 0)
					{
						safe = false;
					}
				}
			}
		}
		if(valid(x,y))
		{
			if(PATH[x][y] > cost)//You've been here before with the same or less cost
			{
				if(GRID[x][y] > 0 && safe)//This is an explored empty region that hasn't been seen yet, go to it's neighbors
				{
					PATH[x][y] = cost;
					que.push(spawn(x+1,y,cost+1));
					que.push(spawn(x-1,y,cost+1));
					que.push(spawn(x,y+1,cost+1));
					que.push(spawn(x,y-1,cost+1));
				} else if(GRID[x][y] == 0 && safe)//This is an unexplored region, call it fog and stop
				{
					fronts.push_back(point(x,y));
					PATH[x][y] = -cost;
				}
				else //this is an impossible region, call it a wall and  stop
				{
					PATH[x][y] = 0;
				}
			}
		}
	}
}

//Converts a coordinate from simulation space to occupancy map space
//Saves the result as the last 2 parameters which were passed by reference
void Map::index(double x, double y, int& outX, int& outY)
{
	if(x < (MINX+1) || y < (MINY+1) || x >= (MAXX-1) || y >= (MAXY-1))
	{
		cout << x << " " << y << " triggered a resize" << endl;
		resize((int)floor(x-3),(int)floor(y-3),(int)ceil(x+3),(int)ceil(y+3));
	}
	outX = (int)((x-MINX)*RES);
	outY = (int)((y-MINY)*RES);
	if(!valid(outX,outY))
	{
		cout << "improper resize for " << x << " " << y << endl;
	}
}

//Converts a coordinate from occupancy map space to simulation space
//Saves the result as the last 2 parameters which were passed by reference
void Map::world(int x, int y, double& outX, double& outY)
{
	outX = MINX+((double)x)/RES;
	outY = MINY+((double)y)/RES;
}

//Prints the map information to a csv file which is read by MATLAB for debugging purposes
void Map::print()
{
	ofstream g, p, b;
	g.open("grid.txt");
	p.open("path.txt");
	b.open("bins.txt");
	for(int j = HEIGHT-1; j >= 0; j--)
	{
		for(int i = 0; i < WIDTH; i++)
		{
			g << (int)GRID[i][j];
			p << (int)PATH[i][j];
			b << (int)BINS[i][j];
			if(i < (WIDTH-1))
			{
				g << ",";
				p << ",";
				b << ",";
			}
		}
		g << endl;
		p << endl;
		b << endl;
	}
	g.close();
	p.close();
	b.close();
}

//Path finding function, input parameters are the robot's current location
//Returns a list of points in simulation coordinates representing the path to a new unexplored region
list<point> Map::path(double x, double y)
{
	cout << "Path started" << endl;
	list<point> path;
	int xIndex, yIndex;
	//If you're too close to the wall go in a random direction
	if(valid(xIndex-RAD,yIndex-RAD) && valid(xIndex+RAD,yIndex+RAD))
	{
		for(int i = xIndex-RAD; i <= (xIndex+RAD); i++)
		{
			for(int j = yIndex-RAD; j <= (yIndex+RAD); j++)
			{
				if(GRID[i][j] < 0)
				{
					world(rand()%WIDTH,rand()%HEIGHT,x,y);
					path.push_front(point(x,y));
					return path;
				}
			}
		}
	}
	index(x,y,xIndex,yIndex);
	//Clears the cost map prior to the path finding
	for(int i = 0; i < WIDTH; i++)
	{
		for(int j = 0; j < HEIGHT; j++)
		{
			PATH[i][j] = INT_MAX;
		}
	}
	vector<point> fronts;
	queue<spawn> que;
	que.push(spawn(xIndex,yIndex,1));
	boardersRec(fronts,que);
	cout << "current position " << x << " " << y << endl;
	point goal(rand()%WIDTH,rand()%HEIGHT);//Create goal node
	if (fronts.size() == 0)//Path finding was unsuccessful at finding viable goal, go to a random unoccupied point on the map
	{
		while(PATH[(int)goal.x][(int)goal.y] > 0)
		{
			goal = point(rand()%WIDTH,rand()%HEIGHT);
		}
	}
	else
	{
		goal = fronts[rand()%fronts.size()];
	}
	int xInd = (int)goal.x;
	int yInd = (int)goal.y;
	int cost = -PATH[xInd][yInd];
	//Finds the route from the goal to the origin based on the cost values given in the path array
	path.push_front(point(xInd,yInd));
	for(int i = 0; i < 10000; i++)//Terminates if infinite loop
	{
		if(!valid(xInd-1,yInd-1) || !valid(xInd+1,yInd+1))
		{
			return path;
		}
		if(PATH[xInd-1][yInd] > 0 && PATH[xInd-1][yInd] < cost)
		{
			xInd--;
		}else if(PATH[xInd+1][yInd] > 0 && PATH[xInd+1][yInd] < cost)
		{
			xInd++;
		}else if(PATH[xInd][yInd-1] > 0 && PATH[xInd][yInd-1] < cost)
		{
			yInd--;
		}else if(PATH[xInd][yInd+1] > 0 && PATH[xInd][yInd+1] < cost)
		{
			yInd++;
		}
		if(xInd == (int)goal.x && yInd == (int)goal.y)
		{
			list<point> fail;
			double x, y;
			world(rand()%WIDTH,rand()%HEIGHT,x,y);
			fail.push_back(point(x,y));
			return fail;
		}
		cost = PATH[xInd][yInd];
		path.push_front(point(xInd,yInd));
		if(PATH[xInd][yInd] == 1)
		{
			break;
		}
	}
	//This removes intermediate steps in the route
	//The only ones which where the robot needs to turn, this creates significant performance improvement with goto() command
	for(list<point>::iterator a = path.begin(); a != path.end(); a++)
	{
		list<point>::iterator b = a;
		b++;
		if(b == path.end())
		{
			break;
		}
		list<point>::iterator c = b;
		c++;
		if(c == path.end())
		{
			break;
		}
		double dx = c->x - a->x;
		double dy = c->y - a->y;
		double length = sqrt(dx*dx+dy*dy);
		dx /= length;
		dy /= length;
		bool pass = true;
		for(double i = 0; i < length; i++)
		{
			if(PATH[(int)(a->x+(i*dx))][(int)(a->y+(i*dy))] == 0)
			{
				pass = false;
			}
		}
		if(pass)
		{
			path.erase(b);
			a--;
		}
	}
	//Converts the coordinates from map coordiantes to simulation coordinates
	for(list<point>::iterator it = path.begin(); it != path.end(); it++)
	{
		double wX, wY;
		world((int)it->x, (int)it->y, wX, wY);
		it->x = wX;
		it->y = wY;
	}
	for(list<point>::iterator it = path.begin(); it != path.end(); it++)
	{
		cout << "node " << it->x << " " << it->y << endl;
	}
	cout << "path done route is " << path.size() << " long" << endl;
	return path;
}

//Marks a sensor reading in the occupancy map using a ray method
//All points from start coordinate to stop coordinate are marked as empty
//If the stop coordinate is sufficiently close, it is marked as a wall collision
void Map::sensor(double startX, double startY, double stopX, double stopY, bool wall)
{
	int startXIndex, startYIndex, stopXIndex, stopYIndex;
	index(startX,startY,startXIndex,startYIndex);
	index(stopX,stopY,stopXIndex,stopYIndex);
	double dx = stopXIndex-startXIndex; double dy = stopYIndex-startYIndex;
	double distance = sqrt((dx*dx)+(dy*dy));
	if(distance > 2.5)
	{
		distance = 2.5;
		wall = false;
	}
	dx /= distance; dy /= distance;
	for(int i = 0;true; i++)
	{
		int xIndex = (int)(startXIndex + (i*dx));
		int yIndex = (int)(startYIndex + (i*dy));
		if(i < distance)
		{
			set(xIndex,yIndex, false, distance);
		}
		else
		{
			if(!wall)
			{
				set(xIndex,yIndex, false, distance);
				return;
			}
			set(xIndex,yIndex, true, distance);
			return;
		}
		if(i > (5*RES))
		{
			return;
		}
	}
}

//Marks a position in the goal detection map which uses a crude implementation of Hough transform
//Angles returned from the range finder are inputed here, An angle between (1.79,1.97)
//is within acceptable error of the 108 degrees for the inner angles of the star
void Map::bin(double x, double y, double rad)
{
	int xIndex, yIndex;
	index(x,y,xIndex,yIndex);
	for(int i = xIndex-2; i <= (xIndex+2); i++)
	{
		for(int j = yIndex-2; j <= (yIndex+2); j++)
		{
			if(valid(i,j))
			{
				set(x,y,true, 1);
				BINS[i][j]++;
				if(rad > 1.79 && rad < 1.97)
				{
					BINS[i][j] += 5;
				}
			}
		}
	}
}

//Tests if the ping from the range finder is close to one of the known walls
//This is needed because the range finder is highly inaccurate but accurate walls are known
bool Map::testPNGWall(double x, double y)
{
	if(WIDTH == 0 || HEIGHT == 0)
	{
		return false;
	}
	int xInd = (int)((x-PNGLEFT)*48.4);
	int yInd = (int)((y-PNGBOTTOM)*48.4);

	for(int i = xInd-TOL; i < (xInd+TOL); i++)
	{
		for(int j = yInd-TOL; j < (yInd+TOL); j++)
		{
			if(i >= 0 && j >= 0 && i < PNGWIDTH && j < PNGHEIGHT)
			{
				if(!PNG[j][i])
				{
					return true;
				}
			}
		}
	}
	return false;
}

//Returns true if the star has been located using the Hough transform and prints it's location
bool Map::star()
{
	spawn largest(0,0,0);
	for(int x = 0; x < WIDTH; x++)
	{
		for(int y = 0; y < HEIGHT; y++)
		{
			if(BINS[x][y] > largest.cost)
			{
				largest.cost = BINS[x][y];
				largest.x = x;
				largest.y = y;
			}
		}
	}
	if(largest.cost > 100)
	{
		double rx, ry;
		world(largest.x,largest.y,rx,ry);
		cout << "STAR FOUND AT APPROXIMATLY " << rx << " , " << ry << endl;
		return true;
	}
	return false;
}
