
all: test

test: final_project.cpp map.cpp main.cpp
	g++ -g -I/usr/include/player-2.0 -L/usr/lib main.cpp lodepng.cpp final_project.cpp map.cpp decode_png.cpp  -lplayerc++ -o final

clean:
	rm -f *.o final
