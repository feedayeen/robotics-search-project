#CSE 180 Final Project

*	Brian Nemec
*	Jeremy Muonio

##Files

1. final_project.h -> contains RobotControl object.
1. final_project.cpp
1. map.h -> contains Map object.
1. map.cpp
1. main.cpp

The Map class is in map.h and map.cpp. Everything else is in final_project.h / final_project.cpp. 

Main.cpp creates a RobotControl object. RobotControl then creates a Map object.

##Changes from Map class.

Almost none.

##Changes from non-Map class parts of final.cc.

Variables used to talk to the robot,

	PlayerClient* robot;
	Position2dProxy* pp;
	LaserProxy* lp;

Are now pointers.

##Requirements

1. Program that drives the robot to the star.
2. Paper.

##Notes From April 8th class:

*	"Star" always the same size.

	*	Other objects will be different sizes.

* 	Robot will be able to fit though all openings.

	* Also able to fit between all objects / walls.

*	Walls will be "straight", all vertical / horizontal on map.

* 	Maps can be different sizes. 

##Posible optimizations later on based on that info

*	Straight walls can be detected easily using the Hough transform
	The minimum size opening will permit fewer samples if we do
	interpolation of close datapoints

##TODO

*   We need to implement a function which can detect the star.
	The range finder's precision will be a problem for this.

*	We need a function return a coordinate of the next region to explore, the mapping
	right now returns a boundary region showing known walls and fronteer edges.
	If you weigh the fronter edges by their distance and target the nearest ones
	we'll have an algorithm better than random walk.
	2dpoint = fun(2dpoint start)

*	We need a function which computes the robot's path from it's current x,y location
	to the goal x,y coordinate without hitting walls. We can generate a new path every
	time a new wall is discovered. The robot's radus needs to be specified to avoid walls.
	Example prototype
	list of 2dpoints = fun(2dpoint start, 2dpoint stop, double rad)
	
##References

*	Pathfinding functions

	http://www.gamasutra.com/view/feature/131505/toward_more_realistic_pathfinding.php?print=1

*	Law of Cosines	-	Very useful for finding the angle between 3 points, easy to implement
	
	Say you have a set of points, ABCDEF. If C is the corner, angles BCD will be equal to the star's angle
	To verify the accuracy of the measurement, ACD,BCD,ACE,ACF,BCF... will all equal the correct angle
	If the distance between point N and C is greater than the length of the star's point, then ignore it
	
	The Hough Transform can also be applied in this situation by having a grid representing every point C where
	this test passes. If sufficiently large number of points within a small area pass, we've found the star

	http://en.wikipedia.org/wiki/Dot_product#Application_to_the_cosine_law

<!-- ##Goal Condition

In "goal_condition_testing.cc", the robot can be controlled by the keyboard. Whenever a key is hit, the goal-condition calculation is preformed.

It uses code from the "lasers" function to generate points from the laser readings. These points are then analyzed using the Law of Cosines.
 -->
<!-- ##PNG Decode.

In "decodePNG.cpp", a PNG file is opened, and converted into a 2d bool vector.
 -->

##Task delegation

C++ PNG

http://lodev.org/lodepng/


##Brian TODO:

1. Pathfinding

2. Map Function


##Jeremy TODO:

1. Read PNG file
	2. Store as 2d bool Array -> if not "white", "black"

2. Figure out the star
