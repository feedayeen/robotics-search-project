// decode_png.h

#include "lodepng.h"
#include <iostream>
#include <vector>

using namespace std;

class DecodePNG
{
public:
	DecodePNG();
	vector<vector < bool> > getPNG(const char* filename);

};

