// map.h

// TODO: Cleanup includes.
#include <iostream>
#include <vector>
#include <list>
#include <queue>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <limits.h>
#include <cmath>
#include <string>
#include <iterator>
#include <sys/time.h>

using namespace std;

//General coordinate used in both simulation space (real) and map bins (discrete)
struct point
{
	double x;
	double y;
	point(int xP, int yP) : x((double)xP), y((double)yP) {}
	point(double xP, double yP) : x(xP), y(yP) {}
};

//This is a struct used for the path finding function
struct spawn
{
	int x;
	int y;
	int cost;
	spawn(int xI, int yI, int cI) : x(xI), y(yI), cost(cI){}
};

class Map
{
private:
	//Map parameters which need to be fine tuned based on the simulation accuracy
	int RES = 25			//Resolution of the map as the bin size
	double RAD = 0.4*RES		//Movement radius of the robot
	double TOL = 0.75*RES	//Tolerance for the range finder identify non-wall obstacles
//Map properties
	int MINX, MINY, MAXX, MAXY, WIDTH, HEIGHT, PNGWIDTH, PNGHEIGHT;
	double PNGLEFT, PNGBOTTOM;
	int** GRID;
	int** PATH;
	int** BINS;
	vector< vector<bool> > PNG;

	void resize(int min_x, int min_y, int max_x, int max_y);
	bool valid(int x, int y);
	void set(int xIndex, int yIndex, bool wall, double dist);
	void boardersRec(vector<point>& fronts, queue<spawn>& que);
	void index(double x, double y, int& outX, int& outY);
	void world(int x, int y, double& outX, double& outY);

public:
	Map(vector< vector<bool> > png, int x, int y, double left, double bottom);
	Map();
	void print();
	list<point> path(double x, double y);
	void sensor(double startX, double startY, double stopX, double stopY, bool wall);
	void bin(double x, double y, double rad);
	bool testPNGWall(double x, double y);
	bool star();
};
